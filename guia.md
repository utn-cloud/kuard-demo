kubectl get pods
# nada por aquí nada por allá
kubectl apply -f kuard-deploy-health-check.yaml  
kubectl get pod
# escalar para mostra
kubectl scale --replicas=2 deploy kuard

# crear el balanceo de carga - altenativa primero sin load balancer
kubectl apply -f kuard-service.yaml  
kubectl get svc -w
# ver IP pública y lo accedan desde el celular

# terminal 1
kubectl get endpoints  --watch  
# terminal 2
kubectl get pods -w
# escalar en otra terminal
kubectl scale --replicas=4 deploy kuard
# terminal 3
#hacer un curl porque sino no se ve que cambia el pod que sirve, por el cache del browser usando el URL del servicio
# curl http://IP-publica |grep host
# repetir para ver como rota el pod que atiende
while :
do  
    curl http://xxx.xxx.xxx.xxx |grep host  
    sleep 20s  
done  
# terminal 4
kubectl port-forward unPodDelTerminal2 8080:8080
# para acceder al pod y ponerlo en falla y/o muerto

# al terminar limpiar todo
k delete svc kuard
k delete deploy kuard

