# practica alternativa al capitulo 7 de Kubernetes Up and Running

kubectl apply -f kuard-deploy-health-check.yaml  
kubectl get pod  
# escalar para mostra
kubectl scale --replicas=2 deploy kuard  
# crear el balanceo de carga
kubectl apply -f kuard-service.yaml  
kubectl get svc -w